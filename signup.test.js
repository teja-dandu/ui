const timeout = 5000

describe(
  '/ (Home Page)',
  () => {
    let page
    beforeAll(async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto('https://stagingapp3.tricaequity.com/login')
    }, timeout)

    afterAll(async () => {
      await page.close()
    })

    it('should load without error', async () => {
      let text = await page.evaluate(() => document.body.textContent)
      expect(text).toContain('welcome to trica equity')
    })

    it('should open a new page', async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto('https://stagingapp3.tricaequity.com/signup')
      let text = await page.evaluate(() => document.body.textContent)
      expect(text).toContain('welcome to trica equity')
    })
    it('should fill firstname input', async () => {
      await expect(page).toFill('input[name="firstName"]', 'teja')

    })
    it('should fill lastname input', async () => {
      await expect(page).toFill('input[name="lastName"]', 'dandu')
    })
    it('should fill an email input', async () => {
      await expect(page).toFill('input[name="email"]', 'teja.dandu+02@trica.co')
    })
    it('should fill an brandname input', async () => {
      await expect(page).toFill('input[name="brandName"]', 'Trica')
    })
    it('should fill telephone number input', async () => {
      await expect(page).toFill('input[type = "tel"]', '8500268905')
    })

    it('should click the submit button', async () => {
      await expect(page).toClick('button', { text: 'continue' })
      // await expect(page).toClick({ type: 'xpath', value: '\\a' }, { text: 'Click' })
    })

    it('should open a new login page', async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto('https://stagingapp3.tricaequity.com/login')
      let text = await page.evaluate(() => document.body.textContent)
      expect(text).toContain('welcome to trica equity')
    })
    it('should fill an email input for login', async () => {
      await expect(page).toFill('input[name="email"]', 'teja.dandu+02@trica.co')
    })
    it('should fill an password input for login', async () => {
      await expect(page).toFill('input[name="password"]', 'Teja@1364')
    })

    it('should click the submit button', async () => {
      await expect(page).toClick('button', { text: 'log in' })
      // await expect(page).toClick({ type: 'xpath', value: '\\a' }, { text: 'Click' })
    })





  },
  timeout
)
